# Simple count of nucleotides given a DNA sequence
import typing

from Bio.Seq import Seq # type: ignore

def count_nucleotides(sequence: Seq) -> typing.Tuple[int, int, int, int]:
    return (
        sequence.count('A'),
        sequence.count('C'),
        sequence.count('G'),
        sequence.count('T')
    )

if __name__ == '__main__':
    test_sequence = Seq('AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC')
    assert count_nucleotides(test_sequence) == (20, 12, 17, 21)

    with open('input/rosalind_dna.txt') as f:
        sequence = Seq(f.read().strip())

    print(count_nucleotides(sequence))
