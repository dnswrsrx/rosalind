import typing
from Bio.Seq import Seq # type: ignore

def complement(sequence: Seq) -> Seq:
    return sequence.reverse_complement()


if __name__ == '__main__':
  test_sequence = Seq('AAAACCCGGT')
  assert complement(test_sequence) == 'ACCGGGTTTT'

  with open('input/rosalind_revc.txt') as f:
    sequence = Seq(f.read().strip())

  print(complement(sequence))
