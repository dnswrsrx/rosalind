import typing

from Bio.Seq import Seq # type: ignore

def motifs(sequence: Seq, motif: str) -> list:
    indices: typing.List[int] = []
    starting_index = 0
    while True:
        index = sequence.find(motif, start=starting_index)
        if index == -1:
            return indices
        starting_index = index + 1
        indices.append(starting_index)


if __name__ == '__main__':
    test_sequence = Seq('GATATATGCATATACTT')
    test_motif = 'ATAT'
    assert motifs(test_sequence, test_motif) == [2, 4, 10]

    with open('input/rosalind_subs.txt') as f:
        sequence, motif = [line.strip() for line in f.readlines()]
        sequence = Seq(sequence)
    print(motifs(sequence, motif))
