import re
import typing

from Bio.SeqUtils import GC # type: ignore


ID_AND_SEQUENCE = re.compile(r'(Rosalind_\d{4})([ACGT]+)')


def highest_gc(fasta_str: str) -> typing.Tuple[str, float]:
    sequences = []
    for id_and_seq in fasta_str.replace(' ', '').replace('\n', '').split('>'):
        match = ID_AND_SEQUENCE.match(id_and_seq)
        if match:
            sequences.append((match.group(1), round(GC(match.group(2)), 6)))
    return max(sequences, key=lambda s: s[1])


if __name__ == '__main__':
    test = '''
    >Rosalind_6404
    CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC
    TCCCACTAATAATTCTGAGG
    >Rosalind_5959
    CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCCGCCGAAGGTCT
    ATATCCATTTGTCAGCAGACACGC
    >Rosalind_0808
    CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGAC
    TGGGAACCTGCGGGCAGTAGGTGGAAT
    '''

    assert highest_gc(test) == ('Rosalind_0808', 60.91954)
    with open('input/rosalind_gc.txt') as f:
        sequences = f.read().strip()
        print(highest_gc(sequences))
