def count_point_mutations(sequence_one: str, sequence_two: str) -> int:
    count = 0
    for n_1, n_2 in zip(sequence_one, sequence_two):
        if n_1 != n_2:
            count +=1
    return count

if __name__ == '__main__':
    test_one = 'GAGCCTACTAACGGGAT'
    test_two = 'CATCGTAATGACGGCCT'
    assert count_point_mutations(test_one, test_two) == 7

    with open('input/rosalind_hamm.txt') as f:
        sequence_one, sequence_two = [l.strip() for l in f.readlines()]

    print(count_point_mutations(sequence_one, sequence_two))

