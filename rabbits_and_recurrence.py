def pairs(months: int, litter=3) -> int:
    population = {'young': 1, 'mature': 0}

    for _ in range(months-1):
        population['mature'] += population['young']
        population['young'] = (population['mature'] - population['young']) * litter

    return sum(population.values())

if __name__ == '__main__':
    assert pairs(5) == 19

    with open('input/rosalind_fib.txt') as f:
        months, litter = [int(x.strip()) for x in f.read().split(' ')]
        print(pairs(months, litter=litter))
