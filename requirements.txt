biopython==1.78
mypy==0.790
mypy-extensions==0.4.3
numpy==1.19.2
typed-ast==1.4.1
typing-extensions==3.7.4.3
