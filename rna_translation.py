import typing

from Bio.Seq import Seq # type: ignore

def translate_rna(sequence: Seq) -> Seq:
    return sequence.translate(stop_symbol='')

if __name__ == '__main__':
    test_sequence = Seq(
        'AUGGCCAUGGCGCCCAGAACUGAGAUCAAUAGUACCCGUAUUAACGGGUGA'
    )
    assert translate_rna(test_sequence) == 'MAMAPRTEINSTRING'

    with open('input/rosalind_prot.txt') as f:
        sequence = Seq(f.read().strip())

    print(translate_rna(sequence))
